"use strict"
const pwd = process.env.PWD
const data = require(pwd + "/" + process.argv[2])
let student_pretenders
if (process.argv[3] == undefined | process.argv[3] == "students") {
    student_pretenders = true
} else if (process.argv[3] == "schools") {
    student_pretenders = false
} else {
    throw "usage: script data.json [students|schools]"
}

// return the rank given from pf_pretender_id to pf_balconers_id
function how_does_pretender_rank_balconer(pf_pretender_id, pf_balconers_id, pf_pretenders_data) {
    for (const pretender of pf_pretenders_data) {
        if (pretender.id == pf_pretender_id) {
            return pretender.preferences[pf_balconers_id]
        }
    }
}

// return the rank given from pf_balconers_id to pf_pretender_id
function how_does_balconer_rank_pretender(pf_balconers_id, pf_pretender_id, pf_on_the_balcony_data) {
    for (const balconer of pf_on_the_balcony_data) {
        if (balconer.id == pf_balconers_id) {
            return balconer.preferences[pf_pretender_id]
        }
    }
}

// return the object with pf_id in a pf_data set
function fecth_object_by_id(pf_id, pf_data) {
    for (const element of pf_data) {
        if (element.id == pf_id) {
            return element
        }
    }
}

// return the id of  pf_rank th pf_querry_id'preference
function fetch_id_from_rank(pf_querry_id, pf_rank, pf_querry_data) {
    for (const querry of pf_querry_data) {
        if (querry.id == pf_querry_id) {
            for (const [key, value] of Object.entries(querry.preferences)) {
                if (value == pf_rank) {
                    return key
                }
            }
        }
    }
}

// return array.max +1
function get_max_incrementation(pf_tableau) {
    return Math.max(...pf_tableau) + 1
}

// Print the final match
function print_match(pf_on_the_balcony_data, pf_pretenders_data, pf_turn_count) {
    console.log("The one standing on the balcony.")
    for (const balconer of pf_on_the_balcony_data) {
        console.log(`${balconer.id} fill its ${balconer.capacity} slot(s) with:`)
        for (const chosen of balconer.balcony_queue) {
            console.log(`- ${chosen.pretender.id} wich was ranked ${how_does_balconer_rank_pretender(balconer.id, chosen.pretender.id, pf_on_the_balcony_data)}`)
        }
        console.log("")
    }
    console.log("The pretenders.")
    for (const pretender of pf_pretenders_data) {
        console.log(`${pretender.id} obtained:`)
        pretender.best_wished.forEach(wish => {
            console.log(`- ${fetch_id_from_rank(pretender.id,wish,pf_pretenders_data)}, wish n°${wish}.`)
        })
        console.log("")
    }

    console.log(`Done in ${pf_turn_count} turn.`)
}

// in this step, all the pretenders go to they pretender.capacity favourite (best wished) and ask for they acceptance
function serenading(pf_pretenders_data, pf_on_the_balcony_data) {
    for (const pretender of pf_pretenders_data) {
        //For each of its wish, the pretender will add itself to its wish balcony queue
        pretender.best_wished.forEach(wish => {
            //It recover the good queue to add itself
            let id = fetch_id_from_rank(pretender.id, wish, pf_pretenders_data)
            let balconer = fecth_object_by_id(id, pf_on_the_balcony_data)
            //It add itself with its rank to the queue
            if (!balconer.balcony_queue.some(tested_pretender => tested_pretender.pretender.id == pretender.id)) {
                balconer.balcony_queue.push({ pretender: pretender, rank: how_does_balconer_rank_pretender(id, pretender.id, pf_on_the_balcony_data) })
            }
        })
    }
}

//In this step, all the balconer reduce they queue, by rejecting the worst ranked, the rejecteds updtades they wish
function rejecting(pf_on_the_balcony_data, pf_pretenders_data) {
    let no_rejection = true
    for (const balconer of pf_on_the_balcony_data) {
        //We sort the queue by the queue owner ranking
        balconer.balcony_queue.sort(compare_pretender)
        //Until the queue match the capacity, we reject the last
        while (balconer.balcony_queue.length > balconer.capacity) {
            let rejected = balconer.balcony_queue.pop().pretender
            //The rejected wish is replaced with the next best
            rejected.best_wished = rejected.best_wished.map(element => {
                if (element == how_does_pretender_rank_balconer(rejected.id, balconer.id, pf_pretenders_data)) {
                    return get_max_incrementation(rejected.best_wished)
                } else {
                    return element
                }
            })
            no_rejection = false
        }
    }
    return no_rejection
}

// function used to sort the array
function compare_pretender(pf_a, pf_b) {
    const a_rank = pf_a.rank
    const b_rank = pf_b.rank
    if (a_rank < b_rank) {
        return -1
    } else if (a_rank > b_rank) {
        return 1
    } else {
        return 0
    }
}

// initialisation 
let pretenders
let on_the_balcony
// by default, all student have a capacity of one.
for (const student of data.students) {
    student.capacity = 1
}

// we fix here who is the pretender and who will be on the balcony
if (student_pretenders) {
    pretenders = data.students
    on_the_balcony = data.schools
} else {
    pretenders = data.schools
    on_the_balcony = data.students
}

// The wish are the best on the start.
for (const pretender of pretenders) {
    pretender.best_wished = []
    for (let index = 1; index <= pretender.capacity; index++) {
        pretender.best_wished.push(index)
    }
}
// All queue are empty.
for (const balconer of on_the_balcony) {
    balconer.balcony_queue = []
}

//Matching ritual algorithm
let everybody_accepted = false
let turn_count = 0
//while people keep getting rejected
while (!everybody_accepted) {
    turn_count++
    //each pretender, go to to his favorite balconer
    serenading(pretenders, on_the_balcony)
    //each balconer, keep in queue only the balconer.capacity favorite, all other are rejected.
    //return false if someone have been rejected.
    //return true if everybody have been accepted
    everybody_accepted = rejecting(on_the_balcony, pretenders)
}
//Print the result
print_match(on_the_balcony, pretenders, turn_count)
