# Graph Project
## Stable wedding algorithm
#### ENSEEIHT - IT Apprenticeship 2022
* Callune Gitennet 
* Maeva Guerrier

Made on January 2020.

#### For Gentian Jakllari
------------------------------------

## Project's goal

The goal of the project was to implement the stabble marriage algorithm, that was learned during Graph courses. The algorithm will be use with schools and students.

## Programming language

We have chosen nodejs beacuse it's an untyped language. We figured it will be easier to use that kind of language since it's more malleable. It allow us to modify our data structure more easily, meaning easier agile developpement. 

Moreover, maeva didn't knew how to use nodejs and was willing to take the chance to learn a new programming language with this project.

------------------------------------

## User Guide

### Instalation
* If you don't have git, get it. With unbuntu: `sudo apt install git`.
* Clone git repository: `git clone https://gitlab.com/n7_appsn2022/school-student-stable_weeding.git`.
* If you don't have [nodeJS](https://nodejs.org/en/), install it. 
    * For unbuntu: `sudo apt install nodejs`. 
    * This is an [Installation guide for mac](https://www.webucator.com/how-to/how-install-nodejs-on-mac.cfm).

### Usage
* Set up your data.json
    * There is [example.json](data/example.json) in the data folder.
    * Your data is a set of 2 groups : students and schools.
    * Each group have an array of set.
    * Each set of the arrays is composed of: 
        * id: must be unique string
        * preferences: a set which all the other group id as key, a unique rank from 1 to the number of entities in the other group as value.
        * For the schools, capacity is the number of students it can recruits.
    ```json
    <!-- A student example -->
    {
        "id": "alban",
        "preferences": {
            "n7": 1,
            "insa": 2,
            "isae": 3,
            "IutBlagnac": 4
        }
    }
    <!-- A school example -->
    {
        "id": "n7",
        "capacity": 2,
        "preferences": {
            "alban": 1,
            "stephan": 2,
            "mélissa": 3,
            "océane": 4,
            "justinien": 5,
            "rahim": 6,
            "maev": 7,
            "otis": 8,
            "olla": 9,
            "lilly": 10
        }
    }
    ```
* Start the software with `node ${path_to_source}/stable_weeding.js ${path to_your_data}.json ${students|schools}`
    * `${path_to_source}` is the path to src dir.
    * `${path to_your_data}` is the **relative** path from your pwd to your json data.
    * `${students|schools}` 'students' or 'schools' is the pretenders, the other group will stand on the balcony.
* The result outpout will go to your terminal. You can redirect it to a file following unix's rules.

------------------------------------

## Data structure

Our schools and students are both object that have similiar attributes. They both have an id which correspond to the student's name or the school's name. Their preferences, based on the available pretenders (which can be etheir the schools or the students) which have been given a rank.

**Here is an example :**

    id: "alban",
    preferences: { n7: 1, insa: 2, isae: 3 IutBlagnac: 4 }
        

You have a student named alban that has given for each school available, a rank. As you can see alban has giver the rank 1 to the school N7. 


    id: "n7",
    capacity: 2,
    preferences: { alban: 1, stephan: 2, mélissa: 3, océane: 4, justinien: 5, rahim: 6, maev: 7, otis: 8, olla: 9, lilly: 10 }
    
You have a school named n7, which has a capacity of 2 students. The school has given for each student available a rank.

**definition**

* **balconer** : is an entity that will stand on a balcony, and wait for the pretender to visit it. Each balconer will keep its capacity favorite among the pretender, and tell the other to leave and not come back tomorrow. 
* **pretender** : is an entity that will stand under the balcony of its capacity favorite balconer among its list. If the balconer reject it it will erase the balconer on its list.
* **best wishes** : is the best ranked whish that the pretender can obtain based on the pretender capacity and the fact that he may have been rejected. For example at the beginning of the algorithm, a pretender A with a capacity of 2 will have an associated list with [1,2]. If the 1 reject him the new best whishes will be [3,2].

In the algorithm the schools and the students can either be a pretender or a balconer.
The pretenders will get a list, that will have the range of the pretender capacity,  which will contained their best wishes.

A balconer after being serenade by the pretenders will have a list called balcony_queue that will contained its favored prentender based on the balconer capacity.

**Let's see an example with the students as pretender and the schools as balconer :** 

    id: "alban",
    preferences: { n7: 1, insa: 2, isae: 3 IutBlagnac: 4 }
    capacity : 1,
    best_wished: [1]

Alban's favorite school his N7 meaning that at the beginning of the algorithm the best wish that he is willing to get is his first wish.

    id: "n7",
    capacity: 2,
    preferences: { alban: 1, stephan: 2, mélissa: 3, océane: 4, justinien: 5, rahim: 6, maev: 7, otis: 8, olla: 9, lilly: 10 }
    balcony_queue: []

At the beginning of the algorithm, no one has yet seen their favorite balconer, meaning that the balconer has an empty list balcony_queue.

**Let's see an example with the schools as pretender and the students as balconer :** 

    id: "n7",
    capacity: 2,
    preferences: { alban: 1, stephan: 2, mélissa: 3, océane: 4, justinien: 5, rahim: 6, maev: 7, otis: 8, olla: 9, lilly: 10 }
    best_wished: [1,2]

In contrary to the student, the schools have a capacity that can be more than one. 
n7's capcity is 2 meaning that at the beginning of the algorithm its list of best_wished will contain its two favorite wish .

    id: "alban",
    preferences: { n7: 1, insa: 2, isae: 3 IutBlagnac: 4 }
    capacity : 1,
    balcony_queue: []


At the beginning of the algorithm, no one has yet seen their favorite balconer, meaning that the balconer has an empty list balcony_queue.

## How we implemented the algorithm

After initialisation, all the pretenders add themself to their **best wished**'s **balcony queue**. We called it **serenading**.
Then all the balconers clean their queue, removing the worst ranked until the queue size match their capacity. It's **rejection** step.
All the rejected pretenders will update their rejected best wished to the next best possible wish. The top ranked not-rejected wish that are not already in queue.
We continue until there is no rejections.

**If you wish you can take a look at the [code](src/stable_weeding.js) everything is commented**

## Encountered difficulties

At first we decided to implement the algorithm with the students as pretenders and the schools on the balcony. However we didn't implemented the fact that schools can take more than one student and we took it into account a bit latter. Since we have chosen nodejs, the changes that needed to be implemented were done easily. 


